﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeLab.Entities
{
    public partial class Student
    {
        public Guid StudentId { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public DateTimeOffset Birthday { get; set; }
    }
}
