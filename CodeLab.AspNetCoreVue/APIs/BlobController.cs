﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeLab.Services;

namespace CodeLab.AspNetCoreVue.APIs
{
    [Route("api/v1/blob")]
    public class BlobController : Controller
    {
        private readonly BlobService BlobService;
        private readonly BarcodeService BarcodeService;

        public BlobController(BlobService blobService, BarcodeService barcodeService)
        {
            this.BlobService = blobService;
            this.BarcodeService = barcodeService;
        }

        [HttpGet("txt-file")]
        public IActionResult GetTxtFile()
        {
            var txtFileBytes = this.BlobService.GenerateTxtFile();

            return File(txtFileBytes, "text/plain", "file.txt");
        }

        [HttpGet("zip-pdf-files")]
        public IActionResult GetPdfFile()
        {
            var pdfFileBytes = this.BlobService.GeneratePdfFilesInZip();

            return File(pdfFileBytes, "application/zip", "file.zip");
        }

        [HttpGet("zip-file")]
        public async Task<IActionResult> GetZipFile()
        {
            var zipFileBytes = await this.BlobService.GenerateZipFile();

            return File(zipFileBytes, "application/zip", "file.zip");
        }

        [HttpGet("barcode")]
        public IActionResult GetBarcode(string content)
        {
            var barcodeBytes = this.BarcodeService.GenerateBarcode128(content);

            return File(barcodeBytes, "image/bmp", "bitmap.bmp");
        }
        
        [HttpGet("qrcode")]
        public IActionResult GetQrCode(string content)
        {
            var qrCodeBytes = this.BarcodeService.GenerateQrCode(content);

            return File(qrCodeBytes, "image/bmp", "bitmap.bmp");
        }
    }
}
