﻿using iTextSharp.text.pdf;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using QRCoder;

namespace CodeLab.Services
{
    public class BarcodeService
    {
        public byte[] GenerateBarcode128(string content)
        {
            var barcode = new Barcode128 {
                CodeType = Barcode.CODE128,
                ChecksumText = true,
                GenerateChecksum = true,
                StartStopText = true,
                Code = content
            };

            var bitmap = new Bitmap(barcode.CreateDrawingImage(Color.Black, Color.White));
            var bitmapBytes = new byte[0];

            using(var ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Bmp);

                bitmapBytes = ms.ToArray();
            }
            
            return bitmapBytes;
        }

        public byte[] GenerateQrCode(string content)
        {
            var qrGenerator = new QRCodeGenerator();
            var qrCodeData = qrGenerator.CreateQrCode(content, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCode(qrCodeData);

            var bitmap = qrCode.GetGraphic(20);
            var bitmapBytes = new byte[0];
            
            using(var ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Bmp);

                bitmapBytes = ms.ToArray();
            }

            return bitmapBytes;
        }
    }
}
