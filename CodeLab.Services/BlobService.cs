﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace CodeLab.Services
{
    public class BlobService
    {
        public byte[] GenerateTxtFile()
        {
            var txtFile = new byte[0];
            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms))
                {
                    sw.WriteLineAsync("Hello World!");
                }
                txtFile = ms.ToArray();
            }

            return txtFile;
        }

        public byte[] GeneratePdfFilesInZip()
        {
            var zipFile = new byte[0];

            using (var ms = new MemoryStream())
            {
                using (var zipStream = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    for (var i = 0; i < 5; i++)
                    {
                        var pdfFile = zipStream.CreateEntry($"File#{ i }.pdf");

                        using (var entryStream = pdfFile.Open())
                        {
                            var pdfDocument = new Document(PageSize.A4);
                            var pdfWriter = PdfWriter.GetInstance(pdfDocument, entryStream);

                            pdfDocument.Open();
                            var table = new PdfPTable(1);
                            table.DefaultCell.Border = 1;
                            table.WidthPercentage = 100;

                            var cell = new PdfPCell();
                            cell.AddElement(new Paragraph($"Hello World { i }!"));

                            table.AddCell(cell);

                            pdfDocument.Add(table);

                            pdfDocument.Close();
                        }
                    }
                }

                zipFile = ms.ToArray();
            }

            return zipFile;
        }

        /// <summary>
        /// Generate a .zip file.
        /// </summary>
        /// <returns>.zip file bytes.</returns>
        public async Task<byte[]> GenerateZipFile()
        {
            var txtFiles = new List<Stream>();
            var zipFile = new byte[0];

            // Open a new MemoryStream.
            using (var ms = new MemoryStream())
            {
                // Using System.IO.Compression.ZipArchives, open a new ZipArchive stream.
                // Why leave it open? Refer to this post: https://forums.asp.net/post/5670767.aspx
                using (var zipStream = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    // Create 5 .txt files.
                    for (var i = 0; i < 5; i++)
                    {
                        var txtFile = zipStream.CreateEntry($"File#{ i }.txt");

                        using (var entryStream = txtFile.Open())
                        {
                            using (var sw = new StreamWriter(entryStream))
                            {
                                await sw.WriteAsync($"Hello World! #{ i }");
                            }
                        }
                    }
                }

                zipFile = ms.ToArray();
            }

            return zipFile;
        }
    }
}
